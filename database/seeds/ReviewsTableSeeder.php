<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('reviews')->delete();
        
        \DB::table('reviews')->insert(array (
            0 => 
            array (
                'id' => 1,
                'book_id' => 2,
                'user_id' => 1,
                'review' => 'hello world',
                'created_at' => '2019-01-16 06:23:40',
                'updated_at' => '2019-01-16 06:23:40',
            ),
            1 => 
            array (
                'id' => 2,
                'book_id' => 2,
                'user_id' => 1,
                'review' => 'Make a model for the categories table
php artisan make:model Category',
                'created_at' => '2019-01-16 06:32:52',
                'updated_at' => '2019-01-16 06:32:52',
            ),
            2 => 
            array (
                'id' => 3,
                'book_id' => 2,
                'user_id' => 1,
                'review' => 'its good to be goooood',
                'created_at' => '2019-01-16 06:33:47',
                'updated_at' => '2019-01-16 06:33:47',
            ),
            3 => 
            array (
                'id' => 6,
                'book_id' => 2,
                'user_id' => 2,
                'review' => 'we',
                'created_at' => '2019-01-16 08:00:23',
                'updated_at' => '2019-01-16 08:00:23',
            ),
            4 => 
            array (
                'id' => 7,
                'book_id' => 7,
                'user_id' => 2,
                'review' => 'hello',
                'created_at' => '2019-01-16 08:02:13',
                'updated_at' => '2019-01-16 08:02:13',
            ),
            5 => 
            array (
                'id' => 8,
                'book_id' => 7,
                'user_id' => 2,
                'review' => '3rd review',
                'created_at' => '2019-01-16 08:03:51',
                'updated_at' => '2019-01-16 08:03:51',
            ),
            6 => 
            array (
                'id' => 9,
                'book_id' => 3,
                'user_id' => 2,
                'review' => 'w
sd
sd
sf',
                'created_at' => '2019-01-16 08:11:06',
                'updated_at' => '2019-01-16 08:11:06',
            ),
            7 => 
            array (
                'id' => 10,
                'book_id' => 2,
                'user_id' => 1,
                'review' => '<p>1 plantaine</p><p>1 gumamela</p><p>1lbs pork</p>',
                'created_at' => '2019-01-16 14:09:08',
                'updated_at' => '2019-01-16 14:09:08',
            ),
            8 => 
            array (
                'id' => 11,
                'book_id' => 2,
                'user_id' => 1,
                'review' => '<ul><li>2lbs of beef</li><li>1 bulb of onion</li><li>2kg chicken thigh</li><li>1 small carrot</li><li>1/4kg potatos</li></ul>',
                'created_at' => '2019-01-16 14:24:48',
                'updated_at' => '2019-01-16 14:24:48',
            ),
        ));
        
        
    }
}