<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'firstname' => 'josh',
                'lastname' => 'mac',
                'username' => 'joshmac',
                'email' => 'josh@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$2oVbUmHrXhm9AyNfTeies.ql.ipwZ/9tU6A.lnpFxr6eme4rlMEAK',
                'remember_token' => 'aJ6tjnrHeADBqteVOizxpsZ7calBf0Tv4j8SAh3sKQzSoy0FtkOM3whMIEVu',
                'created_at' => '2019-01-14 06:46:20',
                'updated_at' => '2019-01-14 06:46:20',
                'role_id' => 1
            ),
            1 => 
            array (
                'id' => 2,
                'firstname' => 'chin',
                'lastname' => 'mac',
                'username' => 'chinmac',
                'email' => 'chin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$sc945osjag3LNvez7SadfOTBJX2oLYStvjlmzB4bG2IsEjkA6x5Qm',
                'remember_token' => NULL,
                'created_at' => '2019-01-16 07:18:32',
                'updated_at' => '2019-01-16 07:18:32',
                'role_id' => 2
            ),
        ));
        
        
    }
}