<?php

use Illuminate\Database\Seeder;
use \App\Book;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::insert([
        	["title"=>"A History of Love", "summary"=>"The History of Love: A Novel is the second novel by the American writer Nicole Krauss, published in 2005. The book was a 2006 finalist for the Orange Prize for Fiction and won the 2008 William Saroyan International Prize for Writing for fiction.", "category_id"=>"3", "image_cover"=>"thehistoryoflove.jpg"],
        	["title"=>"Dune", "summary"=>"Dune is a 1965 science fiction novel by American author Frank Herbert, originally published as two separate serials in Analog magazine. It tied with Roger Zelazny's This Immortal for the Hugo Award in 1966, and it won the inaugural Nebula Award for Best Novel.", "category_id"=>"1", "image_cover"=>"\public\img\dune.jpg"],
        	["title"=>"Gone Girl", "summary"=>"Gone Girl is a thriller novel by the writer Gillian Flynn. It was published by Crown Publishing Group in June 2012. The novel soon made the New York Times Best Seller list. The novel's suspense comes from Nick Dunne, and whether he is involved in the disappearance of his wife.", "category_id"=>"2", "image_cover"=>"\public\img\gonegirl.jpg"]
        ]);
    }
}
