@extends("layouts.layout")

@section("title", "Sample Website - Add Book")

@section("content")
<h1 class="p-4">Add New Book</h1>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<form method="POST" action="/saveItem" enctype="multipart/form-data">
				{{ csrf_field() }} 
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" name="title" id="title" class="form-control">
				</div>
				<div class="form-group">
					<label for="summary">Summary</label>
					<textarea name="summary" id="summary" class="form-control" rows="5"></textarea>
				</div>
				<div class="form-group">
					<label for="category">Category</label>
					<select class="form-control" name="category" id="category">
						@foreach(\App\Category::all() as $category)
						<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="image_cover">Book Cover</label>
					<input type="file" name="image_cover" id="image_cover" class="form-control">
				</div>
				<button type="submit" class="btn btn-primary">Add Book</button>
			</form>
		</div>
	</div>
</div>

@endsection