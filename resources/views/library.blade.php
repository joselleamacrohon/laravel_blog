@extends("layouts.layout")

@section("title", "Sample Website")

@section("content")
<h1 class="p-4">Library Page</h1>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-10 offset-lg-1">
			<div class="row">
				@foreach($books as $book)
					<div class="card col-lg-3 p-4">
						<img src="\{{$book->image_cover}}" class="card-img-top library-book-cover-photo">
						<div class="card-body">
							<h4 class="card-title">{{ $book->title }}</h4>
							<p class="card-text">{{ $book->summary }}</p>
							<p class="card-text"><span class="bg-success py-2 px-2 badge">{{ $book->ownedBy->count() }}</span> <span class="small">added to their collection</span></p>

							<a href="/library/{{$book->id}}" class="btn btn-primary">View Details</a>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>

@endsection