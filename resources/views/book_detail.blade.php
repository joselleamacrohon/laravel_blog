@extends("layouts.layout")

@section("title", "Sample Website")

@section("content")
<h1>Details for {{ $book->title }}</h1>
<div class="container-fluid">
	<p>owners: {{$book_owners->count()}} </p>
			
				<?php if ($user_owns->count() != 0): ?>
					<a href="/library/removeBook/{{$book->id}}" class="btn btn-danger">Remove from your collection</a>
				<?php else: ?>
					<a href="/library/addBook/{{$book->id}}" class="btn btn-success">Add Book</a>
				<?php endif ?>
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<div class="card">
				<img id="book-detail-book-cover-photo" src="/{{$book->image_cover}}" class="card-img-top mx-auto">
				<div class="card-body">
					<h5 class="card-title text-center">{{$book->title}}</h5>
					<p class="card-text text-center">{{$book->summary}}</p>
				</div>
			</div>
		</div>
	</div>
	@auth
	<div class="row">
		<div class="col-lg-4 offset-lg-4 p-3">
			<form action="/library/{{$book->id}}/addReview" method="POST">
				{{ csrf_field() }}
				<label for="review">Add Review</label>
				<!-- <textarea rows="5" class="form-control" id="review" name="review"></textarea> -->
				<textarea class="ckeditor" id="review" name="review"></textarea>
				<button type="submit" class="btn btn-primary my-2">Submit Review</button>

				
{{-- //// --}}
			
				
			</form>
		</div>
	</div>
	@endauth

	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<h3>Reviews</h3>
				@foreach($book->reviewedBy as $review)
				<div class="bg-dark text-white p-3 m-3">
					<!-- $book->reviewedBy gets ALL the users who reviewed the book -->
					<h4>{{$review->username}} said: </h4>
					<h4>$review->id(user): {{$review->id}} </h4>
					<h4>$book->id:{{$book->id}}  </h4>
					<h4>$review->pivot->id(review_id):{{$review->pivot->id}}  </h4>
					@auth
						@if(Auth::user()->id == $review->id)
							<form action="/library/editReview/{{$review->pivot->id}}" method="POST">
								{{ csrf_field() }}
								{{ method_field("PATCH") }}
								<textarea rows="5"  name="review" id="edit_review" class="ckeditor">{{$review->pivot->review}}</textarea>
								<br>
								<button type="submit" class="btn btn-primary">Edit Review</button>
								<a href="/library/deleteReview/{{$review->pivot->id}}" class="btn btn-danger">Delete Review</a>
							</form>
						@else
							<div>
							{{$review->pivot->review}}hello1
							</div>
						@endif
					@else
						<div>
						<?php echo $review->pivot->review; ?>
						</div>
					@endauth
				</div>
				@endforeach

		</div>
	</div>
	
</div>

@endsection