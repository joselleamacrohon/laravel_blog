@extends("layouts.layout")

@section("title", "Sample Website - Profile")

@section("content")
<h1>Profile Page for {{Auth::user()->firstname}}</h1>
<div class="container-fluid">
	
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<h3>Name: {{Auth::user()->firstname}} {{Auth::user()->lastname}}</h3>	
			<h4>Books owned: </h4>
			<table>
				<thead>
					<th>Book Title</th>
					<th>Summary</th>
				</thead>
				<tbody>
					
					@foreach(Auth::user()->owns as $book)
						<tr>
							<td>{{$book->title}}</td>
							<td>{{$book->summary}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>	
	</div>
	
</div>

@endsection