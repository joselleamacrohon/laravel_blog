@extends("layouts.layout")

@section("title", "Sample Website - Admin Dashboard")

@section("content")
<h1 class="p-4">Admin Dashboard</h1>
<a href="\admin\new_book" class="btn btn-primary">New Book</a>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<table class="table table-striped">
				<thead>
					<th>Title</th>
					<th>Summary</th>
					<th>Image</th>
					<th>Actions</th>
				</thead>
				<tbody>
					@foreach($books as $book)
					<tr>
						<td>{{ $book->title }}</td>
						<td>{{ $book->summary}}</td>
						<td><img src="/{{$book->image_cover}}" style="height:50; width: 50px;"></td>
						<td>
							<a href="/admin/edit/{{$book->id}}" class="btn btn-primary">Edit</a>
							<button class="btn-danger btn" data-toggle="modal" data-target="#confirmdelete">Delete</button>
							<!-- <a href="/admin/delete/{{$book->id}}" class="btn btn-danger">delete</a> -->
						</td>
					</tr>


					<!-- delete modal -->
					<div id="confirmdelete" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4>Confirm Delete</h4>
								</div>
								<div class="modal-body">
									<p>Do you want to delete the item?</p>
								</div>
								<div class="modal-footer">
									<a href="/admin/delete/{{$book->id}}" class="btn btn-danger">delete</a>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</div>
					</div>

					@endforeach
				</tbody>
				
			</table>
		</div>
	</div>
</div>

@endsection