@extends("layouts.layout")

@section("title", "Sample Website - Edit Book")

@section("content")
<h1 class="p-4">Edit Details for book: {{$book->title}}</h1>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<form method="POST" action="/admin/edit/{{$book->id}}" enctype="multipart/form-data">
				{{ csrf_field() }} 
				{{ method_field("PATCH") }}
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" name="title" id="title" class="form-control" value="{{$book->title}}">
				</div>
				<div class="form-group">
					<label for="summary">Summary</label>
					<textarea name="summary" id="summary" class="form-control" rows="5">{{$book->summary}}</textarea>
				</div>
				<div class="form-group">
					<label for="category">Category</label>
					<select class="form-control" name="category" id="category">
						@foreach(\App\Category::all() as $category)
						<option value="{{ $category->id }}" {{$category->id == $book->category_id ? "selected":""}}>{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="image_cover">Book Cover</label>
					<input type="file" name="image_cover" id="image_cover" class="form-control p-1">
				</div>
				<button type="submit" class="btn btn-primary">Update Book</button>
			</form>
		</div>
	</div>
</div>

@endsection