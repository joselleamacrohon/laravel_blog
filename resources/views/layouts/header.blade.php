<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a href="#" class="navbar-brand">Sample Site</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarContent">
		<ul class="navbar-nav ml-auto">
			@guest
			<li class="nav-item">
				<a href="{{ route("login") }}" class="nav-link"> Login </a>
			</li>
			<li class="nav-item">
				<a href="{{ route("register") }}" class="nav-link"> Register </a>
			</li>
			@else
			<li class="nav-item">
				<a href="/profile" class="nav-link">
					Welcome, {{ Auth::user()->firstname }}! 
				</a>
			</li>
			<li class="nav-item">
				<a href="{{ route('logout') }}" onlclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout </a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">
					{{csrf_field()}}
				</form>
			</li>
			@endguest
		</ul>
	</div>
</nav>