<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Admin pages
Route::middleware("admin")->group(function(){
	Route::get('/admin', "BookController@showAdminPage");
	Route::get('/admin/new_book','BookController@showNewBookForm');
	Route::post('/saveItem', 'BookController@saveBook');
	Route::get('/admin/delete/{id}','BookController@deleteBook');
	Route::get('/admin/edit/{id}','BookController@showEditForm');
	Route::patch('/admin/edit/{id}', 'BookController@editBook');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/error', 'BookController@showError');
Route::get('/library', "BookController@showBooks");
Route::get('/library/{id}', "BookController@showBookDetail");


//users
Route::middleware("auth")->group(function(){
	Route::post('/library/{id}/addReview', 'BookController@addReview');
	Route::patch('library/editReview/{id}', 'BookController@editReview');
	Route::get('/library/deleteReview/{id}', 'BookController@deleteReview');
	Route::get('/library/addBook/{id}', 'BookController@addToCollection');
	Route::get('/library/removeBook/{id}', 'BookController@removeFromCollection');
	Route::get('/profile','BookController@showProfile');

}); //end middleware

