<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'name', 'email', 'password',
        'firstname', 'lastname', 'username', 'email', 'password','role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function reviewed(){
        //link users to books via the reviews table
        return $this->belongsToMany("\App\Book", "reviews")
        ->withPivot("id","review")
        ->withTimestamps(); 
        
    }

    public function owns(){
        //link users to books via the book_user table ->(default table name)
        return $this->belongsToMany("\App\Book")
        ->withTimestamps(); 
        
    }


}
