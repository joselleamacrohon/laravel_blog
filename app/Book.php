<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //reviews
    public function reviewedBy(){
    	return $this->belongsToMany("\App\User", "reviews") //tablename
    	->withPivot("id","review")  //columns
    	->withTimestamps(); 
    	//by adding a 2nd argument to belongsToMany we 
    	//override the convention of the table it's going to look at

    	// if we user this
    	// $this->belongsToMany("\App\User");
    	//this assumes that laravel looks for a table called "book_user"
    }

    //ownernership 
    //refers to the book_user which is the default pivot table name for laravel

    public function ownedBy(){
        return $this->belongsToMany("\App\User") //tablename
        ->withTimestamps(); 
    }




}
