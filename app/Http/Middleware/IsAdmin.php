<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        if(Auth::user() && Auth::user()->role_id == 1){
            return $next($request); //this allows the user to proceed to the 
                                    //next page, meaning the middleware test
                                    //has been satisfied
        } else {
            return redirect("/error");
        }

    }
}
