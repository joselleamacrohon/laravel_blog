<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Book;
use Auth;
use DB; //this allows us to access tables without the use of models

class BookController extends Controller
{
    //
    public function showError(){
    	return view("error");
    }

    public function showBooks(){
    	$books = Book::all();
    	return view("library", compact("books"));
    }

    public function showBookDetail($id){
    	$book = Book::find($id);
        $user_id = Auth::user()->id;

        $user_owns = DB::table("book_user")
                            ->where("book_id", $id)
                            ->where("user_id", $user_id)->get();

        //find book_id in book_user - to know if anyone added to collection
        $book_owners = DB::table("book_user")
                            ->where("book_id", $id)->get();


    	return view("book_detail", compact("book","user_owns","book_owners"));
    }

    public function showAdminPage(){
    	$books = Book::all();
    	return view("admin_dashboard", compact("books"));
    }

    public function showNewBookForm(){
    	return view("add_book_form");
    }

    public function saveBook(Request $request){
    	$rules = array(
    		"title" => "required",
    		"summary" => "required",
    		"category" => "required",
    		"image_cover" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048"
    	);

    	$this->validate($request, $rules);
    	//TODO: include $errors in the form
    	$book = new Book;
    	$book->title = $request->title;
    	$book->summary = $request->summary;
    	$book->category_id = $request->category;
    	
    	//process image
    	$image = $request->file("image_cover");
    	$image_name = time().".".$image->getClientOriginalExtension();
    	$destination = "img/";
    	$image->move($destination, $image_name);
    	$book->image_cover = $destination.$image_name;
    	
    	$book->save();
    	return redirect("/admin");
    }

    public function deleteBook($id) {
    	$book = Book::find($id);
    	$book->delete();
    	return redirect("/admin");
    }

    public function showEditForm($id){
    	$book = Book::find($id);
    	return view("edit_form", compact("book"));

    }

    public function editBook($id, Request $request){

    	$book = Book::find($id);
    	
    	//todo: validation
    	$rules = array(
    		"title" => "required",
    		"summary" => "required",
    		"category" => "required",
    		"image_cover" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048"
    	);

    	$this->validate($request, $rules);

    	$book->title = $request->title;
    	$book->summary = $request->summary;
    	$book->category_id = $request->category;
    	
    	if($request->file("image_cover") != null){
    		$image = $request->file("image_cover");
    		$image_name = time().".".$image->getClientOriginalExtension();
    		$destination = "img/";
    		$image->move($destination, $image_name);
    		$book->image_cover = $destination.$image_name;
    	}

    	$book->save();
    	return redirect("/admin");

    }

    public function addReview($id, Request $request){
    	$book = Book::find($id); //finds the book to be reviewed
    	$user = Auth::user(); //gets the currently logged-in user
							//meaning there needs to be a user logged in before a review can be made
    	// note: used AUTH so dont forget to "use auth above"

		$book->reviewedBy()->attach($user->id, ["review"=> $request->review]);
		//insert into table:reviews (book_id, user_id, review) values ($book->id, $user->id, $request->review)    	
		//look for the book, call the method that links books and users, attach the user_id, and the pivot content.
		//it automatically saves the item

		// other way to do it: -since they have the same relationship
		// $user->reviewed()->attached($id, ["review"=>$request->review]);

		return redirect("/library/$id");
    }

    public function editReview($id, Request $request){
    	DB::table("reviews")->where("id", $id)->update(["review"=>$request->review]);
    	return redirect()->back();
    }

    public function deleteReview($id){
    	// Auth::user()->reviewed()->wherePivot("id", $id)->detach();
    	DB::table("reviews")->where("id", $id)->delete();
    	return redirect()->back();
    }
    //***********Other way of DELETING
    // use href="/library/deleteReview/{{$book->id}}/{{$review->pivot->id}}" --> in book_details.blade
    // use route::get('/library/deleteReview/{bookid}/{reviewid}','BookController@deleteReview')
    // public function deleteReview($bookid, $reviewid){
    // 	$book = Book::find($bookid);
    // 	$book->reviewedBy()->wherePivot("id", $reviewid)->detach();
    // 	return redirect()->back();
    // }
    //Note: we Need to go first to the parent to delete



    public function addTOCollection($id){
        $user = Auth::user()->id;
        $book = Book::find($id);

        $book->ownedBy()->attach($user);
        return redirect("/library/$id");
    } 

    public function removeFromCollection($id){
        $user = Auth::user()->id;
        
        DB::table("book_user")->where("book_id", $id)
                            ->where("user_id", $user)->delete();
        
        return redirect("/library/$id");
    } 

    public function showProfile(){
        //its not necessary to call Auth::user() here and pass it to the view
        //because we could directly user Auth::user() in the view itself
        return view("profile");        
    }



}
